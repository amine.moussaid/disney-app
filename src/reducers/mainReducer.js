import Image1 from '../images/image1.jpg'
import Image2 from '../images/image2.jpg'
import Image3 from '../images/image3.jpg'
import Image4 from '../images/image4.jpg'

import { ADD_TO_CART, ADD_CUSTOMER } from '../actions/action-types/index'

const initState = {
  offers: [
    {
      id: '1',
      title: 'Titre de l\'offre 1',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eleifend nec nulla sit amet elementum. Morbi rhoncus augue ac ligula sagittis hendrerit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse sollicitudin malesuada ultrices. Curabitur vel pretium magna.',
      img: Image1,
      price: 400
    },
    {
      id: '2',
      title: 'Titre de l\'offre 2',
      description: 'Proin euismod sed ligula nec pretium. Etiam egestas tempor nunc ut hendrerit. Suspendisse aliquet massa sed mi viverra vulputate. In hac habitasse platea dictumst.',
      img: Image2,
      price: 500
    },
    {
      id: '3',
      title: 'Titre de l\'offre 3',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      img: Image3,
      price: 300
    },
    {
      id: '4',
      title: 'Titre de l\'offre 4',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus malesuada neque nec euismod auctor. Morbi sed odio ipsum. In augue lacus, bibendum nec odio at, lobortis malesuada urna. Nulla sapien mauris, convallis non vestibulum id, ultricies a ante. Proin accumsan sem in nunc congue ornare in egestas lectus.',
      img: Image4,
      price: 700
    }
  ],
  addedItems: [],
  total: 0
}
const cartReducer = (state = initState, action) => {
  if (action.type === ADD_TO_CART) {
    let addedItem = state.offers.find(offer => offer.id === action.id)
    //check if the action id exists in the addedItems
    let existed_offer = state.addedItems.find(offer => action.id === offer.id)
    if (existed_offer) {
      addedItem.quantity += 1
      return {
        ...state,
        total: state.total + addedItem.price
      }
    }
    else {
      addedItem.quantity = 1;
      //calculate the total
      let newTotal = state.total + addedItem.price

      return {
        ...state,
        addedItems: [...state.addedItems, addedItem],
        total: newTotal
      }

    }
  }
  if (action.type === ADD_CUSTOMER) {
    return {
      ...state,
      customer: {
        firstName: action.payload.firstName,
        lastName: action.payload.lastName,
        email: action.payload.email,
        adress: action.payload.adress
      }
    }
  }


  return state;

}
export default cartReducer;