
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addToCart } from '../actions/index';

class Home extends Component {
  handleClick = (id) => {
    this.props.addToCart(id);
  }

  handleClickCheckout = () => {
    this.props.history.push('/checkout/step1');
  }

  render() {
    let offerList = this.props.offers.map(offer => {
      return (
        <div className="card horizontal m-content" key={offer.id}>
          <div className="card-image valign-wrapper">
            <img src={offer.img} alt={offer.title} />
          </div>

          <div className="card-content">

            <div className="card-row">
              <span className="card-title">{offer.title}</span>
              <p className="center"><b>{offer.price}€</b><br />prix/nuit</p>
            </div>

            <p>{offer.description}</p>

          </div>
          <span to="/" className="btn-floating halfway-fab waves-effect waves-light" onClick={() => { this.handleClick(offer.id) }}><i className="material-icons">add</i></span>

        </div>


      )
    })

    let cartList = this.props.addedItems.map(offer => {
      return (
        <div className="card horizontal c-content" key={offer.id}>
          <div className="card-image valign-wrapper">
            <img src={offer.img} alt={offer.title} />
          </div>

          <div className="card-content">

            <div className="card-row">
              <span className="card-title"></span>
              <p className="center"><b>{offer.price}€</b><br />prix/nuit</p>
            </div>

          </div>

        </div>
      )
    })

    return (
      <div className="row container">
        <div className="col s8 ">
          <div className="containe">
            <h5 className="start">Nos Offres :</h5>
            <div className="box">
              {offerList}
            </div>
          </div>

        </div>
        <div className="col s4 grey lighten-3 cart-wrapper">
          {/* Cart Content */}
          <h4>PANIER</h4>
          {cartList}
          <h4 className="green lighten-4 right-align">Total : {this.props.total} €</h4>
          <div className="col s12 center">
            <button className="btn waves-effect waves-light mt-5 mb-5" type="submit" name="action" onClick={this.handleClickCheckout}>Checkout
              <i className="material-icons right">send</i>
            </button>
          </div>
        </div>
      </div>

    )
  }
}
const mapStateToProps = (state) => {
  return {
    offers: state.offers,
    addedItems: state.addedItems,
    total: state.total
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (id) => { dispatch(addToCart(id)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)