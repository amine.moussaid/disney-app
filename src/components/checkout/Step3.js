
import React from 'react';

const Step3 = () => {
  return (
    <div className="container center">
      <h4 className="mt-20">🎉 Félicitations ! 🎉</h4>
      <h5 className="mt-5">Votre commande a été enregistrée </h5>
      <h1>✔️</h1>
    </div >
  )

}


export default Step3
