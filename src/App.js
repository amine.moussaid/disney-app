import React, { Component } from 'react';
// import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Navbar from './components/Navbar';
import Home from './components/Home';
import Step1 from './components/checkout/Step1';
import Step2 from './components/checkout/Step2';
import Step3 from './components/checkout/Step3';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">

          <Navbar />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/checkout/step1" component={Step1} />
            <Route path="/checkout/step2" component={Step2} />
            <Route path="/checkout/step3" component={Step3} />
          </Switch>
        </div>
      </BrowserRouter>

    );
  }
}

export default App;
