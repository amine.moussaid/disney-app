
import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
  return (
    <nav className="nav-wrapper grey">
      <div className="container">
        <Link to="/" className="brand-logo">Logo</Link>

        <ul className="right">

          <a className="user-btn">
            <i className="user-icon material-icons">account_circle</i>
            John Doe
          </a>

        </ul>
      </div>
    </nav >
  )
}

export default Navbar;