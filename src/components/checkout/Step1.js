
import React, { Component } from 'react';

import { connect } from 'react-redux';
import { addCustomer } from '../../actions/index';

class Step1 extends Component {

  constructor() {
    super();

    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      adress: ""
    };

  }

  handleChange = event => {
    this.setState({ [event.target.id]: event.target.value });
  }

  handleSubmit = event => {
    event.preventDefault();
    console.log("submitted");
    const payload = this.state;
    this.props.addCustomer(payload)
    this.props.history.push('/checkout/step2')
  }


  render() {
    const { firstName, lastName, email, adress } = this.state;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>

          <div className="input-field col s12 mt-20">
            <input id="firstName" type="text" className="validate"
              value={firstName}
              onChange={this.handleChange} />
            <label htmlFor="firstName">Prénom</label>
            <span className="helper-text" data-error="wrong" data-success=""></span>
          </div>

          <div className="input-field col s12 mt-5">
            <input id="lastName" type="text" className="validate"
              value={lastName}
              onChange={this.handleChange} />
            <label htmlFor="lastName">Nom</label>
            <span className="helper-text" data-error="Wrong !" data-success=""></span>
          </div>

          <div className="input-field col s12 mt-5">
            <input id="email" type="email" className="validate"
              value={email}
              onChange={this.handleChange} required />
            <label htmlFor="email">Email</label>
            <span className="helper-text" data-error="Veuillez siasir une adresse e-mail valide." data-success=""></span>
          </div>

          <div className="input-field col s12 mt-5">
            <input id="adress" type="text" className="validate"
              value={adress}
              onChange={this.handleChange} />
            <label htmlFor="adress">Adresse</label>
            <span className="helper-text" data-error="Wrong !" data-success=""></span>
          </div>

          <div className="col s12 center">
            <button className="btn waves-effect waves-light mt-5" type="submit" name="action">Suivant
          <i className="material-icons right">send</i>
            </button>
          </div>
        </form>


      </div >
    )
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    addCustomer: (payload) => { dispatch(addCustomer(payload)) }
  }
}

export default connect(null, mapDispatchToProps)(Step1)
