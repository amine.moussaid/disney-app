
import React, { Component } from 'react';

import { connect } from 'react-redux';

class Step2 extends Component {

  handleClick = () => {
    this.props.history.push('/checkout/step3');
  }



  render() {
    let bookings = this.props.addedItems.map((element) => {
      return (
        <tr>
          <td>{element.title}</td>
          <td>{element.quantity} nuit(s)</td>
          <td>{element.price} €</td>
        </tr>
      )
    })

    let { total, customer } = this.props;
    return (
      <div className="container">
        <h4>Récapitulatif de la commande :</h4>
        <table className="mt-5">
          <tr>
            <th>Prénom :</th>
            <td>{customer.firstName}</td>
          </tr>
          <tr>
            <th>Nom :</th>
            <td>{customer.lastName}</td>
          </tr>
          <tr>
            <th>E-mail :</th>
            <td>{customer.email}</td>
          </tr>
          <tr>
            <th>Adresse :</th>
            <td>{customer.adress}</td>
          </tr>
        </table>
        <table className="mt-5">
          <thead>
            <tr>
              <th>Nom de l'hôtel</th>
              <th>Nom de nuits</th>
              <th>Prix par nuit</th>
            </tr>
          </thead>

          <tbody>
            {bookings}
            <tr>
              <th></th>
              <th style={{ textAlign: 'end' }}>Total :</th>
              <th>{total} €</th>
            </tr>
          </tbody>

        </table>

        <div className="col s12 center mt-5">
          <button className="btn waves-effect waves-light mt-5" type="submit" name="action" onClick={this.handleClick}>Terminer
          <i className="material-icons right">send</i>
          </button>
        </div>



      </div >
    )
  }
}

const mapStateToProps = (state) => {
  return {
    addedItems: state.addedItems,
    total: state.total,
    customer: state.customer
  }
}


export default connect(mapStateToProps)(Step2)
