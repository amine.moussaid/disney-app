import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import mainReducer from './reducers/mainReducer';

import { Provider } from 'react-redux';
import { createStore } from 'redux';

const store = createStore(
  mainReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

