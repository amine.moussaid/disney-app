import * as types from './action-types/index'

export const addToCart = (id) => {
  return {
    type: types.ADD_TO_CART,
    id
  }
}

export const addCustomer = (payload) => {
  return {
    type: types.ADD_CUSTOMER,
    payload
  }
}